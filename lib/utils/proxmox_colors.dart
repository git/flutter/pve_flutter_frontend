import 'package:flutter/material.dart';

class ProxmoxColors {
  static const Color orange = Color(0xFFE57000);
  static const Color black = Color(0xFF000000);
  static const Color supportLightOrange = Color(0xFFFF9100);
  static const Color supportBlue = Color(0xFF00617F);
  static const Color supportDarkGrey = Color(0xFF464D4D);

  // generated from material colors spec
  static const Color blue900 = Color(0xFF003951);
  static const Color blue800 = Color(0xFF004e69);
  static const Color blue700 = Color(0xFF00617F); // actual proxmox support blue
  static const Color blue600 = Color(0xFF1b7696);
  static const Color blue500 = Color(0xFF2c85a8);
  static const Color blue400 = Color(0xFF4e98b9);
  static const Color blue300 = Color(0xFF6aaccb);
  static const Color blue200 = Color(0xFF8cc5e2);
  static const Color blue100 = Color(0xFFabdff7);
  static const Color blue50 = Color(0xFFcdf4ff);

  // chosen with the values.js lib, but still needs to be weeded out and
  // may get improved.
  static const Color grey = Color(0xFF464D4D);
  static const Color greyTint20 = Color(0xFF6b7171);
  static const Color greyTint40 = Color(0xFF909494);
  static const Color greyTint60 = Color(0xFFb5b8b8);
  static const Color greyTint80 = Color(0xFFdadbdb);
  static const Color greyShade20 = Color(0xFF383e3e);
  static const Color greyShade40 = Color(0xFF2a2e2e);
  static const Color greyShade60 = Color(0xFF1c1f1f);
  static const Color greyShade80 = Color(0xFF0e0f0f);

  static const Color supportGrey = Color(0xFFABBABA);
  static const Color supportGreyTint25 = Color(0xFFc0cbcb);
  static const Color supportGreyTint50 = Color(0xFFd5dddd);
  static const Color supportGreyTint75 = Color(0xFFeaeeee);
  static const Color supportGreyShade25 = Color(0xFF808c8c);
  static const Color supportGreyShade50 = Color(0xFF565d5d);
  static const Color supportGreyShade75 = Color(0xFF2b2f2f);
}
