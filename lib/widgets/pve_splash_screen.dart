import 'package:flutter/material.dart';

class PveSplashScreen extends StatelessWidget {
  const PveSplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Image.asset('assets/images/Proxmox_logo_white_orange_800.png');
  }
}
