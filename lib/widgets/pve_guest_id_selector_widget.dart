import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:pve_flutter_frontend/bloc/pve_guest_id_selector_bloc.dart';
import 'package:pve_flutter_frontend/states/proxmox_form_field_state.dart';

class PveGuestIdSelector extends StatelessWidget {
  final String? labelText;

  const PveGuestIdSelector({
    super.key,
    this.labelText,
  });

  @override
  Widget build(BuildContext context) {
    final pveGuestIdSelectorBloc = Provider.of<PveGuestIdSelectorBloc>(context);
    return StreamBuilder<PveFormFieldState>(
        stream: pveGuestIdSelectorBloc.state,
        initialData: pveGuestIdSelectorBloc.state.value,
        builder: (context, snapshot) {
          final state = snapshot.data!;

          return TextFormField(
            // make sure a new internal state is created if the
            // first build has no data
            key: state.value != null ? null : const ValueKey(1),
            decoration: InputDecoration(labelText: labelText, helperText: ' '),
            initialValue: state.value,
            keyboardType: TextInputType.number,
            inputFormatters: [FilteringTextInputFormatter.digitsOnly],
            autovalidateMode: AutovalidateMode.onUserInteraction,
            onChanged: (text) {
              pveGuestIdSelectorBloc.events.add(OnChanged(text));
            },
            validator: (_) {
              return state.errorText;
            },
          );
        });
  }
}
