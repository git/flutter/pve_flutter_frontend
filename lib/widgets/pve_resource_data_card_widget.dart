import 'package:flutter/material.dart';

class PveResourceDataCardWidget extends StatelessWidget {
  final bool showTitleTrailing;
  final bool expandable;
  final Widget title;
  final Widget? titleTrailing;
  final Widget? subtitle;
  final EdgeInsets padding;
  final List<Widget> children;

  const PveResourceDataCardWidget({
    super.key,
    this.showTitleTrailing = false,
    required this.title,
    this.titleTrailing,
    this.subtitle,
    required this.children,
    this.padding = const EdgeInsets.all(8.0),
    this.expandable = false,
  });
  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Padding(
      padding: padding,
      child: Theme(
        data: theme.copyWith(
          listTileTheme: theme.listTileTheme.copyWith(
            textColor: theme.colorScheme.onSurface,
            selectedColor: theme.colorScheme.onSurface,
          ),
        ),
        child: Card(
          color: Theme.of(context).colorScheme.surface,
          clipBehavior: Clip.antiAlias,
          child: Builder(
            builder: (context) {
              if (expandable) {
                return ExpansionTile(
                  shape: const RoundedRectangleBorder(),
                  textColor: theme.colorScheme.onSurface,
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      title,
                      if (showTitleTrailing) titleTrailing!,
                    ],
                  ),
                  subtitle: subtitle,
                  children: children,
                );
              }
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ListTile(
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        title,
                        if (showTitleTrailing) titleTrailing!,
                      ],
                    ),
                    subtitle: subtitle,
                  ),
                  const Divider(
                    indent: 10,
                    endIndent: 10,
                  ),
                  ...children
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
