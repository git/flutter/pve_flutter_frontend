import 'package:flutter/widgets.dart';

class ProxmoxIcons {
  ProxmoxIcons._();

  static const _kFontFam = 'ProxmoxIcons';

  static const IconData proxmox = IconData(0xf101, fontFamily: _kFontFam);
}
